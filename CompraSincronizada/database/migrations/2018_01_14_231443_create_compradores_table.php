<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompradoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compradores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('comprador');
            $table->timestamps();
        });
        $compradores = new \App\Compradores();
        $compradores->comprador = "Big cat";
        $compradores->save();

        $compradores = new \App\Compradores();
        $compradores->comprador = "little cat";
        $compradores->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compradores');
    }
}
