<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCentrosComercialesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centros_comerciales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('centro_comercial');
            $table->integer('cantidad_entrada_id')->unsigned();
            $table->timestamps();

            $table->foreign('cantidad_entrada_id')->references('id')->on('cantidad_de_entradas')->ondelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('centros_comerciales');
    }
}
