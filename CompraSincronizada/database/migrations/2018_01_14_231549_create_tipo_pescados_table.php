<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoPescadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_pescados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo_pescado');
            $table->integer('centro_comercial_id')->unsigned();
            $table->integer('cantidad_entrada_id')->unsigned();
            $table->timestamps();

            $table->foreign('centro_comercial_id')->references('id')->on('centros_comerciales')->ondelete('cascade');
            $table->foreign('cantidad_entrada_id')->references('id')->on('cantidad_de_entradas')->ondelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_pescados');
    }
}
