<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePescadosVendidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pescados_vendidos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cantidad');
            $table->integer('tipo_pescado_id')->unsigned();
            $table->timestamps();

            $table->foreign('tipo_pescado_id')->references('id')->on('tipo_pescados')->ondelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pescados_vendidos');
    }
}
