<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarreterasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carreteras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('carretera');
            $table->integer('tiempo');
            $table->integer('cantidad_entrada_id')->unsigned();
            $table->integer('comprador_id')->unsigned();

            $table->foreign('cantidad_entrada_id')->references('id')
                ->on('cantidad_de_entradas')->ondelete('cascade');
            $table->foreign('comprador_id')->references('id')
                ->on('compradores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carreteras');
    }
}
