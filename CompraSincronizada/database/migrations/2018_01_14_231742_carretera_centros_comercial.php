<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CarreteraCentrosComercial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carretera_centros_comercial', function (Blueprint $table) {
            $table->integer('centro_comercial_id')->unsigned();
            $table->integer('caretera_id')->unsigned();

            $table->foreign('centro_comercial_id')->references('id')->on('centros_comerciales');
            $table->foreign('caretera_id')->references('id')->on('carreteras');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carretera_centros_comercial');
    }
}
