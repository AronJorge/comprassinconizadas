<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compradores extends Model
{
    protected $fillable = [
        'id',
        'comprador'
    ];

    function carreteras()
    {
        return $this->hasMany(Carretera::class);
    }
}
