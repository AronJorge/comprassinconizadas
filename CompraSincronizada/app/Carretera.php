<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carretera extends Model
{
    protected $fillable = [
        'carretera',
        'tiempo',
        'cantidad_entrada_id',
        'comprador_id',
    ];

    function cantidadEntradas()
    {
        return $this->belongsTo(CantidadDeEntrada::class);
    }

    function centroComerciales()
    {
        return $this->belongsToMany(CentrosComerciale::class, 'carretera_centros_comercial', 'caretera_id', 'centro_comercial_id');
    }

    function compradores()
    {
        return $this->belongsTo(Compradores::class);
    }
}
