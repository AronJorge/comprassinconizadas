<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CantidadDeEntrada extends Model
{
    protected $fillable = [
        'centro_comercial',
        'carretera',
        'tipo_pescado',
    ];

    function carreteras(){
        return $this->hasMany(Carretera::class);
    }


    function centroComerciales(){
        return $this->hasMany(CentrosComerciale::class);
    }

    function tipoPescados(){
        return $this->hasMany(TipoPescados::class);
    }
}
