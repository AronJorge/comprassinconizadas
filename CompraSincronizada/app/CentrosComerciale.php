<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CentrosComerciale extends Model
{
    protected $fillable = [
        'centro_comercial',
        'tiempo',
        'cantidad_entrada_id',
    ];

    function cantidadEntradas(){
        return $this->belongsTo(CantidadDeEntrada::class);
    }

    function tipoPescados()
    {
        return $this->hasMany(TipoPescados::class);
    }

    function carreteras(){
        return $this->belongsToMany(Carretera::class,'carretera_centros_comercial','centro_comercial_id','caretera_id');
    }
}
