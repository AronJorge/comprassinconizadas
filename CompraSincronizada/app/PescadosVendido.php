<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PescadosVendido extends Model
{
    protected $fillable = [
        'cantidad',
        'tipo_pescado_id'
    ];

    function tipoPescados(){
        return $this->belongsTo(TipoPescados::class);
    }
}
