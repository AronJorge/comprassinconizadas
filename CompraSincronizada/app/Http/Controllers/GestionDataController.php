<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Operaciones\FragmentacionDataEntrada;
use App\Http\Controllers\Operaciones\Restrinciones;
use Illuminate\Http\Request;

class GestionDataController extends Controller
{
    public function store(Request $request)
    {
        $data = $request->all();

        $restrinciones = new Restrinciones();
        $resp = $restrinciones->Validacion($data['data_entrada']);
        if ($resp != "") {
            return $resp;
        } else {
            $fragmentado = new FragmentacionDataEntrada();
            $tiempoRecorrido = $fragmentado->Extraer_GuardarNKM($data['data_entrada']);
            return $tiempoRecorrido;
        }

    }
}
