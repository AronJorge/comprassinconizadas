<?php

namespace App\Http\Controllers\Operaciones;

use App\CantidadDeEntrada;
use App\Carretera;
use App\CentrosComerciale;
use App\Compradores;
use App\PescadosVendido;
use App\TipoPescados;
use App\Http\Controllers\Controller;

class FragmentacionDataEntrada extends Controller
{
    function Extraer_GuardarNKM($dataEntrada)
    {
        $N = $dataEntrada[0][0];
        $M = $dataEntrada[0][1];
        $K = $dataEntrada[0][2];

        $cantidadEntrada = new CantidadDeEntrada();
        $cantidadEntrada->centro_comercial = $N;
        $cantidadEntrada->carretera = $M;
        $cantidadEntrada->tipo_pescado = $K;
        $cantidadEntrada->save();

        $idsCentrosComerciales = $this->GuardarCentroComercial($cantidadEntrada);
        $this->GuardarCantidadTipoPeces($dataEntrada, $cantidadEntrada, $idsCentrosComerciales);
        $tiempoRecorrido = $this->GuardarCarretera($dataEntrada, $cantidadEntrada);
        return ['tiempo_recorrido' => $tiempoRecorrido];
    }


    function GuardarCentroComercial($cantidadEntrada)
    {
        $idsCentrosComerciales = array();
        for ($i = 1; $i <= $cantidadEntrada->centro_comercial; $i++) {
            $centroComercial = new CentrosComerciale();
            $centroComercial->centro_comercial = $i;
            $centroComercial->cantidad_entrada_id = $cantidadEntrada->id;
            $centroComercial->save();
            array_push($idsCentrosComerciales, $centroComercial->id);
        }
        //dd($idsCentrosComerciales);
        return $idsCentrosComerciales;
    }

    function GuardarCantidadTipoPeces($dataEntrada, $cantidadEntrada, $idsCentrosComerciales)
    {
        $K = $cantidadEntrada->tipo_pescado;
        for ($i = 1; $i <= $K; $i++) {
            $GuardandoTipoPeces = new TipoPescados();
            $GuardandoTipoPeces->tipo_pescado = $dataEntrada[$i][1];
            $GuardandoTipoPeces->centro_comercial_id = $idsCentrosComerciales[rand(0, count($idsCentrosComerciales) - 1)];
            $GuardandoTipoPeces->cantidad_entrada_id = $cantidadEntrada->id;
            $GuardandoTipoPeces->save();

            $GuardandoPecesVendidos = new PescadosVendido();
            $GuardandoPecesVendidos->cantidad = $dataEntrada[$i][0];
            $GuardandoPecesVendidos->tipo_pescado_id = $GuardandoTipoPeces->id;
            $GuardandoPecesVendidos->save();

        }
    }

    function GuardarCarretera($dataEntrada, $cantidadEntrada)
    {
        $bigCat = 0;
        $littleCat = 0;

        $compradoresCat = Compradores::all();
        $incrementadorPociociones = $cantidadEntrada->tipo_pescado + 1;

        for ($i = 1; $i <= $cantidadEntrada->carretera; $i++) {
            $carretera = new Carretera();
            $carretera->carretera = $i;
            $carretera->tiempo = $dataEntrada[$incrementadorPociociones][2];
            $carretera->cantidad_entrada_id = $cantidadEntrada->id;
            $carretera->comprador_id = $compradoresCat[$i % 2]->id;
            $carretera->save();

            $cc = CentrosComerciale::whereIn('centro_comercial', [$dataEntrada[$incrementadorPociociones][0], $dataEntrada[$incrementadorPociociones][1]])
                ->where('cantidad_entrada_id', '=', $cantidadEntrada->id)->get();

            //dd($cc);
            $carretera->centroComerciales()->sync([$cc[0]->id, $cc[1]->id]);

            if (($i % 2)) {
                $littleCat = $littleCat + $dataEntrada[$incrementadorPociociones][2];
            } else {
                $bigCat = $bigCat + $dataEntrada[$incrementadorPociociones][2];
            }

            $incrementadorPociociones++;
        }
        $data = ['BigaCat' => $bigCat, 'littleCat' => $littleCat];
        return max($data);
    }
}
