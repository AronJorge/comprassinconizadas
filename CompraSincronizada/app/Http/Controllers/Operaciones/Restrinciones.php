<?php

namespace App\Http\Controllers\Operaciones;


class Restrinciones
{
    function Validacion($dataEntrada)
    {
        $N = $dataEntrada[0][0];
        $M = $dataEntrada[0][1];
        $K = $dataEntrada[0][2];

        $MaximoN = pow(10, 3);
        $MaximoM = 2 * pow(10, 3);
        $MaximoK = 10;

        if (2 <= $N && $N <= $MaximoN && 1 <= $M && $M <= $MaximoM && 1 <= $K && $K <= $MaximoK) {
            for ($i = 1; $i <= $K; $i++) {
                $verificados = array();
                if (0 <= $dataEntrada[$i][0] && $dataEntrada[$i][0] <= $K && 1 <= $dataEntrada[$i][1] && $dataEntrada[$i][1] <= $K && 2 == count($dataEntrada[$i])) {
                    if (!in_array($dataEntrada[$i][0], $verificados)) {
                        array_push($verificados, $dataEntrada[$i][0]);
                    } else {
                        return ["Respuesta" => "Los Tipos de pescado no pueden repetirse o los datos de entradas son incorrectos.", "Error" => "Linea" . $i];
                    }
                } else {
                    return ["Respuesta" => "El cantidad de pescados o el tipo supera el numero de tipos de pescado (K) o los datos de entradas son incorrectos.", "Error" => "Linea" . $i];
                }
            }


            $iniciaLineaM = $K + 1;
            $cantidadLineasM = count($dataEntrada)-$iniciaLineaM ;
            for ($j = $iniciaLineaM; $j < count($dataEntrada); $j++) {

                if (1 <= $dataEntrada[$j][0] && 1 <= $dataEntrada[$j][1] && $dataEntrada[$j][0] <= $N && $dataEntrada[$j][1] <= $N && $dataEntrada[$j][0] != $dataEntrada[$j][1] &&  $cantidadLineasM === $M && 3 == count($dataEntrada[$i])) {
                    if (1 <= $dataEntrada[$j][2] && $dataEntrada[$j][2] <= pow(10, 4)) {
                    } else {
                        return ["Respuesta" => "El tiempo de viaje supera el limite", "Error" => "Linea " . ($iniciaLineaM+$i)];
                    }
                } else {
                    return ["Respuesta" => "Un numero de centro comercial supera la cantidad de centros comerciales (N) o sus numeros son iguales o los datos de entradas son incorrectos.", "Error" => "Linea " . ($iniciaLineaM+$i)];
                }
            }

        } else {
            return "Los datos de la primera linea superan la cantidad limite (N,M,K) o los datos de entradas son incorrectos.";
        }
    }
}
