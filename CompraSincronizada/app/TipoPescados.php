<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoPescados extends Model
{
    protected $fillable = [
        'tipo_pescado',
        'centro_comercial_id',
        'cantidad_entrada_id',
    ];

    function cantidadEntradas()
    {
        return $this->belongsTo(CantidadDeEntrada::class);
    }

    function centrosComerciales()
    {
        return $this->belongsTo(CentrosComerciale::class);
    }

    function pecadosVendido()
    {
        return $this->hasMany(PescadosVendido::class);
    }
}
